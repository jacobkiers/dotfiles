#!/bin/bash
if [ -z "$SSH_AUTH_SOCK" ] ; then
    eval `ssh-agent -s`
fi

KEY="$HOME/.ssh/work"

[ -f $KEY ] || return;

ssh-add -l | grep $KEY > /dev/null || ssh-add $KEY

