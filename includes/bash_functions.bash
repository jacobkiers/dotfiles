# Print cyan underlined header 
function header() {
	echo -e "$UNDERLINE$CYAN$1$NOCOLOR"
}

# Create a new directory and enter it
function md() {
	mkdir -p "$@" && cd "$@"
}

# cd into whatever is the forefront Finder window.
cdf() {
	cd "`osascript -e 'tell app "Finder" to POSIX path of (insertion location as alias)'`"
}

# Find shorthand
function f() {
	find . -name "$1" 2>/dev/null
}

# Quick grep: ag (+sack), ack or grep
if command -v sag >/dev/null 2>&1; then alias g="sack -ag"
elif command -v ag >/dev/null 2>&1; then alias g="ag -i"
elif command -v ack >/dev/null 2>&1; then alias g="ack -ri"
else alias g="grep -ri"; fi

# Compare original and gzipped file size
function gz() {
	local origsize=$(wc -c < "$1")
	local gzipsize=$(gzip -c "$1" | wc -c)
	local ratio=$(echo "$gzipsize * 100/ $origsize" | bc -l)
	printf "Original: %d bytes\n" "$origsize"
	printf "Gzipped: %d bytes (%2.2f%%)\n" "$gzipsize" "$ratio"
}

# Test if HTTP compression (RFC 2616 + SDCH) is enabled for a given URL.
# Send a fake UA string for sites that sniff it instead of using the Accept-Encoding header. (Looking at you, ajax.googleapis.com!)
function httpcompression() {
	encoding="$(curl -LIs -H 'User-Agent: Mozilla/5 Gecko' -H 'Accept-Encoding: gzip,deflate,compress,sdch' "$1" | grep '^Content-Encoding:')" && echo "$1 is encoded using ${encoding#* }" || echo "$1 is not using any encoding"
}

# Show HTTP headers for given URL
# Usage: headers <URL>
# https://github.com/rtomayko/dotfiles/blob/rtomayko/bin/headers
function headers() {
	curl -sv -H "User-Agent: Mozilla/5 Gecko" "$@" 2>&1 >/dev/null |
		grep -v "^\*" |
		grep -v "^}" |
		cut -c3-
}

# Remove screenshots from desktop
function cleandesktop() {
	header "Cleaning desktop..."
	for file in ~/Desktop/Screen\ Shot*.png; do
		unlink "$file"
	done
	echo
}

# Extract archives of various types
function extract() {
	if [ -f $1 ] ; then
		local dir_name=${1%.*}  # Filename without extension
		case $1 in
			*.tar.bz2)  tar xjf           $1 ;;
			*.tar.gz)   tar xzf           $1 ;;
			*.tar.xz)   tar Jxvf          $1 ;;
			*.tar)      tar xf            $1 ;;
			*.tbz2)     tar xjf           $1 ;;
			*.tgz)      tar xzf           $1 ;;
			*.bz2)      bunzip2           $1 ;;
			*.rar)      unrar x           $1 $2 ;;
			*.gz)       gunzip            $1 ;;
			*.zip)      unzip -d$dir_name $1 ;;
			*.Z)        uncompress        $1 ;;
			*)          echo "'$1' cannot be extracted via extract()" ;;
		esac
	else
		echo "'$1' is not a valid file"
	fi
}

# Print nyan cat
# https://github.com/steckel/Git-Nyan-Graph/blob/master/nyan.sh
# If you want big animated version: `telnet miku.acm.uiuc.edu`
function nyan() {
	echo
	echo -en $RED'-_-_-_-_-_-_-_'
	echo -e $NOCOLOR$BOLD',------,'$NOCOLOR
	echo -en $YELLOW'_-_-_-_-_-_-_-'
	echo -e $NOCOLOR$BOLD'|   /\_/\\'$NOCOLOR
	echo -en $GREEN'-_-_-_-_-_-_-'
	echo -e $NOCOLOR$BOLD'~|__( ^ .^)'$NOCOLOR
	echo -en $CYAN'-_-_-_-_-_-_-'
	echo -e $NOCOLOR$BOLD'""  ""'$NOCOLOR
	echo
}

# Copy public SSH key to clipboard. Generate it if necessary
ssh-key() {
	file="$HOME/.ssh/id_rsa.pub"
	if [ ! -f "$file" ]; then
		ssh-keygen -t rsa
	fi
	
	cat "$file"
}

# Create an SSH key and uploads it to the given host
# Based on https://gist.github.com/1761938
ssh-add-host() {
	username=$1
	hostname=$2
	identifier=$3

	if [[ "$identifier" == "" ]] || [[ "$username" == "" ]] || [[ "$hostname" == "" ]]; then
		echo "Usage: ssh-add-host <username> <hostname> <identifier>"
	else
		header "Generating key..."
		if [ ! -f "$HOME/.ssh/$identifier.id_rsa" ]; then
			ssh-keygen -f ~/.ssh/$identifier.id_rsa -C "$USER $(date +'%Y/%m%/%d %H:%M:%S')"
		fi

		if ! grep -Fxiq "host $identifier" "$HOME/.ssh/config"; then
			echo -e "Host $identifier\n\tHostName $hostname\n\tUser $username\n\tIdentityFile ~/.ssh/$identifier.id_rsa" >> ~/.ssh/config
		fi

		header "Uploading key..."
		ssh $identifier 'mkdir -p .ssh && cat >> ~/.ssh/authorized_keys' < ~/.ssh/$identifier.id_rsa.pub

		tput bold; ssh -o PasswordAuthentication=no $identifier true && { tput setaf 2; echo "SSH key added."; } || { tput setaf 1; echo "Failure"; }; tput sgr0

		_ssh_reload_autocomplete
	fi
}

# Find files with Windows line endings (and convert then to Unix in force mode)
# USAGE: crlf [file] [--force]
function crlf() {
	local force=

	# Single file
	if [ "$1" != "" ] && [ "$1" != "--force" ]; then
		[ "$2" == "--force" ] && force=1 || force=0
		_crlf_file $1 $force
		return
	fi

	# All files
	[ "$1" == "--force" ] && force=1 || force=0
	for file in $(find . -type f -not -path "*/.git/*" -not -path "*/node_modules/*" | xargs file | grep ASCII | cut -d: -f1); do
		_crlf_file $file $force
	done
}
function _crlf_file() {
	grep -q $'\x0D' "$1" && echo "$1" && [ $2 ] && dos2unix "$1"
}

function git-work()
{
    if [ -s ~/.gitlocal ]; then
        unlink ~/.gitlocal
    fi

    ln -s ~/.gitlocal_work ~/.gitlocal
}

function git-private()
{
    if [ -s ~/.gitlocal ]; then
        unlink ~/.gitlocal
    fi

    ln -s ~/.gitlocal_private ~/.gitlocal
}

# Add special aliases that will copy result to clipboard (escape → escape+)
for cmd in password hex2hsl hex2rgb escape codepoint ssh-key myip; do
	eval "function $cmd+() { $cmd \$@ | c; }"
done

function jira()
{
    open "https://comandi.atlassian.net/browse/CMD-${1}"
}
