#!/usr/bin/env bash
pushd "$HOME/dotfiles" > /dev/null || exit 1;

CURRENT_HASH=`md5sum update.sh | awk '{ print $1 }'`

echo -n "Pulling changes... "
git pull

NEW_HASH=`md5sum update.sh | awk '{ print $1 }'`
if [ "$CURRENT_HASH" != "$NEW_HASH" ]
then
    echo "The update script has changed."
    ./update
    exit
fi

echo "Syncing..."
./sync.py

source "$HOME/.bashrc"

if [ ! -f bin/composer ]
then
    echo "Installing composer..."
    wget https://getcomposer.org/composer.phar -O bin/composer
    chmod +x bin/composer
fi

echo -n "Updating composer and dependencies... "
bin/composer --quiet self-update
bin/composer --quiet global require \
    fabpot/php-cs-fixer \
    phpmd/phpmd \
    phpunit/phpunit \
    squizlabs/php_codesniffer

echo "Done."

if [ ! -L resources/git-template/hooks ]
then
    ln -s $HOME/dotfiles/resources/git-hooks resources/git-template/hooks
fi

popd > /dev/null