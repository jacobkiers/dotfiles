#!/bin/bash

# Installs Homebrew, Git, git-extras, git-friendly, hub, Node.js, configures Apache, PHP, MySQL, etc.

# Ask for the administrator password upfront
sudo -v

# Install Homebrew
command -v brew >/dev/null 2>&1 || ruby -e "$(curl -fsSkL raw.github.com/mxcl/homebrew/go)"

# Make sure we’re using the latest Homebrew
brew update

# Upgrade any already-installed formulae
brew upgrade

# GNU core utilities (those that come with OS X are outdated)
brew install coreutils
# GNU `find`, `locate`, `updatedb`, and `xargs`, g-prefixed
brew install findutils
brew install tree

# More recent versions of some OS X tools
brew tap homebrew/dupes
brew install homebrew/dupes/grep

# Git
brew install git
brew install git-extras
brew install hub
sudo bash < <( curl https://raw.github.com/jamiew/git-friendly/master/install.sh ~/Documents/src/bin)

# Ag, the better ack, which is the better grep
brew install the_silver_searcher
brew install https://raw.github.com/nybblr/homebrew-dev/master/sack.rb

# Python
brew install python
sudo pip install fabric

brew install gist

# Remove outdated versions from the cellar
brew cleanup
