#!/bin/bash

if command -v brew >/dev/null 2>&1; then
    brew install caskroom/cask/brew-cask

    brew cask install alfred
    brew cask install arq
    brew cask install firefox
    brew cask install google-chrome
    brew cask install hipchat
    brew cask install iterm2
    brew cask install java
    brew cask install libreoffice
    brew cask install phpstorm
    brew cask install skype
    brew cask install slack
fi

if command -v apt >/dev/null 2>&1; then

    sudo -v

    sudo apt install -y apt-transport-https ca-certificates curl software-properties-common

    # Add repositories

    ## Hipchat
    sudo sh -c 'echo "deb https://atlassian.artifactoryonline.com/atlassian/hipchat-apt-client $(lsb_release -c -s) main" > /etc/apt/sources.list.d/atlassian-hipchat4.list'

    ## Ondrej PHP PPA
    sudo add-apt-repository -y ppa:ondrej/php

    ## Docker
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

    ## Virtualbox
    curl -fsSL https://www.virtualbox.org/download/oracle_vbox_2016.asc | sudo apt-key add -
    sudo add-apt-repository -y "deb http://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib"

    sudo apt update

    # Install apt packages
    sudo apt install -y \
        chromium-browser \
        dkms virtualbox-5.1 \
        dnsmasq \
        docker-ce \
        git \
        hipchat \
        htop \
        jq \
        linux-image-extra-$(uname -r) linux-image-extra-virtual \
        mysql-workbench \
        openjdk-9-jdk openjdk-8-jdk \
        scdaemon pcscd libccid \
        transmission \
        vim \
        vlc

    ## Docker Compose
    if [ ! -f /usr/local/bin/docker-compose ]; then
        DOCKER_COMPOSE_VERSION=1.16.1
        sudo curl -L https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose && sudo chmod +x /usr/local/bin/docker-compose
        sudo curl -L https://raw.githubusercontent.com/docker/compose/${DOCKER_COMPOSE_VERSION}/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
    fi

    ## Vagrant
    if [ ! -f /usr/bin/vagrant ]; then
        sudo curl -L https://releases.hashicorp.com/vagrant/2.0.0/vagrant_2.0.0_x86_64.deb -o vagrant.deb && sudo dpkg -i vagrant.deb && rm -rf vagrant.deb
    fi

fi
